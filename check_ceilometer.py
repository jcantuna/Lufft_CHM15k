# author : Juan Carlos Antuña-Sanchez
# email : jcantuna@goa.uva.es
# date : 11/10/20
# -*- coding: utf-8
# ------------------------------------
import ast
import os
import socket
import smtp_send as mail

# Ceilometer IP address
ip = 'ip_address'

# Read error codes from file
file = open("error_codes.txt", "r")
contents = file.read()
errors_codes = ast.literal_eval(contents)
file.close()

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Check if ceilometer is online. Try 5 times.
rep = os.system('ping ' + ip + ' -c 5')
if rep == 0:
    print("The ceilometer is online")
else:
    mail.send_alert_mail("offline")

# Check ceilometer status
ceilo = os.popen('curl -s --data "rawdata" http://' + ip + '/cgi-bin/chm-cgi').read()
status_code = ceilo[17:25]
if errors_codes[status_code] != 'OK':
    mail.send_alert_mail(errors_codes[status_code])
